import { Container } from 'next/app'
import React from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import { reducer as indexReducer } from '../reducers'


interface Props {

}

const rootReducer = combineReducers({
    index: indexReducer,
  })
  
  export type AppState = ReturnType<typeof rootReducer>


export default
  class MyApp extends React.Component<Props & any> {
    render () {
      const { Component, pageProps } = this.props
      return (
        <Container>
          <Provider store={ createStore(rootReducer, {}, composeWithDevTools(applyMiddleware(thunkMiddleware))) }>
            <Component {...pageProps} />
          </Provider>
        </Container>
      )
    }
  }