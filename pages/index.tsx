import React, {Component} from 'react';
import {
  Container, Row, Col, Card, CardBody,
  FormGroup, Input, Form, Label, Button
} from 'reactstrap';
import Footer from '../components/Footer'
import MyNavbar from '../components/MyNavbar'
import "../assets/scss/argon-dashboard-react.scss";
import Head from 'next/head'
import TodoList from '../components/TodoList';
import { connect } from 'react-redux'
import { TodoItem } from '../reducers';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { addTodo, deleteTodo } from '../actions';
import { AppState } from './_app';



interface IndexPageState{
  addTitle:string,
  addText: string
}

export class IndexPage extends Component<{} & StateProps & DispatchProps, IndexPageState > {

constructor(props: {} & StateProps & DispatchProps){
  super(props);
  this.state = {
    addTitle: '',
    addText: ''
  }
  this.onItemDeleteClick=this.onItemDeleteClick.bind(this)
  this.handleAddTitleChange = this.handleAddTitleChange.bind(this)
  this.handleAddTextChange = this.handleAddTextChange.bind(this)
}

onAddClick(){
  this.props.add(this.state.addTitle, this.state.addText)
}

onItemDeleteClick(id: number){
  this.props.delete(id);
 
}

handleAddTitleChange(event: any) {
  this.setState({addTitle: event.target.value});
}


handleAddTextChange(event: any) {
  this.setState({addText: event.target.value});
}

  render () {
    return (
   <>
      <Head>
        <title>Todo list example</title>
        <link href="/all.min.css" rel="stylesheet" />
        <link href="/nucleo.css" rel="stylesheet" />
      </Head>
      <div className="main-content">
        <MyNavbar />
        <div className="header bg-gradient-info py-7 py-lg-8">
          <Container>
            <div className="header-body text-center mb-7">
              <Row className="justify-content-center">
                <Col lg="5" md="6">
                  <h1 className="text-white">Next.js, Redux, Typescript, Reactstrap, Sass, Storybook, Storyshot & Jest Example!</h1>
                  <p className="text-lead text-light">
                    Use this example to develop modern, testable, Storybook driven frontend.
                </p>
                </Col>
              </Row>
            </div>
          </Container>
          <div className="separator separator-bottom separator-skew zindex-100">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="fill-default"
                points="2560 0 2560 100 0 100"
              />
            </svg>
          </div>
        </div>
        {/* Page content */}
        <Container className="mt--8 pb-5">
          <Row className="justify-content-center">
            <Col xs="3">


              <Card>
                <CardBody>
                  <Form onSubmit={e => {
                    e.preventDefault()
                    
                  }}>
                    <FormGroup>
                      <Label for="title">Title</Label>
                      <Input onChange={this.handleAddTitleChange} value={this.state.addTitle} type="text" name="title" id="title" placeholder="title" />
                    </FormGroup>

                    <FormGroup>
                      <Label for="text">Text</Label>
                      <Input onChange={this.handleAddTextChange} value={this.state.addText} rows='5' type="textarea" name="text" id="text" />
                    </FormGroup>

                    <Button onClick={()=> this.onAddClick() } className={"float-right"}>Add</Button>
                  </Form>
                </CardBody>
              </Card>


            </Col>
            <Col>
            <TodoList items={this.props.items} onItemDeleteClick={this.onItemDeleteClick}/>
            
            </Col>
          </Row>
        </Container>
      </div>
      <Footer />
    </>
    )}
}


interface StateProps {
  items: TodoItem[];
}
     
interface DispatchProps {
  add: (title: string, text: string) => void
  delete: (id: number)  => void,
}

const mapStateToProps = (state: any): StateProps => ({
  items: (state as AppState).index.items,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>): DispatchProps => {
  return {
    add: (title: string, text: string) => dispatch(addTodo(title, text)),
    delete: (id: number) => dispatch(deleteTodo(id)),
  };
};

export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(IndexPage)


