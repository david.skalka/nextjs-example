// components/Button.stories.tsx
import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, object } from '@storybook/addon-knobs';
import { IndexPage } from '../../pages';
import { action } from '@storybook/addon-actions'

storiesOf('IndexPage', module).addDecorator(withKnobs).add('default', () => {
  return <IndexPage items={object('items', [{id: 1 , title: 'Lorem ipsum dolor sit amet', text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'}])} add={action('add')} delete={action('delete')}/>
})