// components/Button.stories.tsx
import * as React from 'react'
import { storiesOf } from '@storybook/react'
import TodoItem from '../../components/TodoItem'
import { action } from '@storybook/addon-actions'
import { withKnobs,text } from '@storybook/addon-knobs';
import centered from '@storybook/addon-centered/react';
storiesOf('TodoItem', module).addDecorator(centered).addDecorator(withKnobs).add('default', () => {
  return <TodoItem title={text('Title', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')} text={text('Text', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')} id={22} onDeleteClick={action('onDeleteClick')}/>
})
