// components/Button.stories.tsx
import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs';
import TodoList from '../../components/TodoList';


storiesOf('TodoList', module).addDecorator(withKnobs).add('default', () => {
  return <TodoList items={object('Items', [{id:0, title:"Lorem ipsum,", text:"Lorem ipsum dolor sit amet"}, {id:1, title:"Lorem ipsum dolor", text:"Lorem ipsum dolor sit amet"}])} onItemDeleteClick={action('onItemDeleteClick')}/>
})
