// components/Button.stories.tsx
import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs';
import MyNavbar from '../../components/MyNavbar';
import centered from '@storybook/addon-centered/react';

storiesOf('MyNavbar', module).addDecorator(centered).addDecorator(withKnobs).add('default', () => {
  return <div style={{backgroundColor:"#11cdef"}}><MyNavbar /></div>
})