import React from "react";
import { Container, Row, Col } from "reactstrap";
import TodoItem from "./TodoItem";

type TodoListProps = {
  items: {id: number, title: string, text: string}[]
  onItemDeleteClick: any;
}


class TodoList extends React.Component<TodoListProps> {
  render() {
    return (
      <>
        <Container>

          {this.props.items.map(item => (
            <Row key={item.id} className={"mb-5"}>
              <Col>
                <TodoItem  {...item} onDeleteClick={this.props.onItemDeleteClick}   />
              </Col>
            </Row>
          ))}

        </Container>
      </>
    );
  }
}
export default TodoList;