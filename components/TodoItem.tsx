import React from "react";
import { CardBody, CardTitle, Card, CardText, CardHeader } from "reactstrap";
import { IoMdTrash } from 'react-icons/io';
import '../styles/TodoItem.scss'

export type TodoItemProps = {
  title: string,
  text: string,
  id: number,
  onDeleteClick: any
}

class TodoItem extends React.Component<TodoItemProps> {
  render() {
    return (
      <>
        <Card className="todo-item">
          <CardHeader>
            <IoMdTrash className={"float-right delete-icon"} onClick={() => this.props.onDeleteClick(this.props.id)} ></IoMdTrash>
          </CardHeader>
          <CardBody>
            <img src="/git.svg" className="float-left rounded-circle mr-4 avatar-img" />
            <CardTitle><h3>{this.props.title}</h3></CardTitle>
            <CardText>{this.props.text}</CardText>
          </CardBody>
        </Card>
      </>
    );
  }
}
export default TodoItem;