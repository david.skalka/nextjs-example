# Next.js example

This is Next.js, Redux, Typescript, Reactstrap, Sass, Storybook, Storyshot & Jest Todo list example application focused on Storybook driven development, covered using full automated Jest snapshot testing (StoryShots).

[![pipeline status](https://gitlab.com/david.skalka/nextjs-example/badges/master/pipeline.svg)](https://gitlab.com/david.skalka/nextjs-example/commits/master)
[![codacy](https://img.shields.io/codacy/grade/c9496e25cf07434cba786b462cb15f49/master.svg)](https://www.codacy.com)
[![coverage report](https://gitlab.com/david.skalka/nextjs-example/badges/master/coverage.svg)](https://gitlab.com/david.skalka/nextjs-example/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)



<img src="readme-images/main-screenshot.png"  width="80%" >

## Getting Started


```
git clone https://gitlab.com/david.skalka/nextjs-example.git
cd nextjs-example
npm i
```

### For Storybook development 
Developing components in isolation

```
npm run storybook
open localhost:6006
```

<img src="readme-images/storybook-screenshot.png"  width="60%" >


### For fullstack development

```
npm run dev
open localhost:3000
```



### Prerequisites

install Node.js 10 & npm, also you can see to .gitlab-ci.yml for prerequisites details

```
open https://nodejs.org/en/download/
open https://www.npmjs.com/get-npm
```


## Running the tests

`npm run test`

### Snapshot tests

> Snapshot tests are a very useful tool whenever you want to make sure your UI does not change unexpectedly. 

#### Update snapshots (confirm changes)
`npm run update-snapshot`


## Running the tests coverage
`npm run test-coverage`

![Coverage screenshot](readme-images/coverage.png)

Check your package.json for coverage treshold.

```
{
  ...
  "jest": {
    "coverageThreshold": {
      "global": {
        "branches": 50,
        "functions": 50,
        "lines": 50,
        "statements": 50
      }
    }
  }
}
```



## Built With

* [Next.js](https://nextjs.org) -  The React Framework
* [Redux](https://redux.js.org//) -  A predictable state container for JavaScript apps.
* [Typescript](https://www.typescriptlang.org/) -  JavaScript that scales
* [Reactstrap](https://reactstrap.github.io/) -  React Bootstrap 4 components
* [Sass](https://reactstrap.github.io/) -  CSS with superpowers
* [Storybook](https://storybook.js.org/) -  UI component explorer for frontend developers
* [StoryShots](https://www.npmjs.com/package/@storybook/addon-storyshots) -  automatic Jest Snapshot Testing for Storybook
* [Jest](https://jestjs.io) -  Delightful JavaScript Testing



## Authors

* **David Skalka** - *Initial work* - david.skalka@gmail.com


## License

This project is licensed under the MIT License

