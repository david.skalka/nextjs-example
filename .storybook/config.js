import { configure } from '@storybook/react';


function areWeTestingWithJest() {
  return process.env.JEST_WORKER_ID !== undefined;
}

if(!areWeTestingWithJest()){
  require("../assets/scss/argon-dashboard-react.scss")
}

const req = require.context('../stories/', true, /stories\.tsx$/)

function loadStories() {
  req.keys().forEach(req)
}

configure(loadStories, module);