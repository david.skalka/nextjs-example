import { actionTypes } from "../reducers";

export function addTodo(title: string, text: string) {
  return {
    type: actionTypes.ADD,
    payload : {title, text}
  }
}


export function deleteTodo(id: number) {
  return {
    type: actionTypes.DELETE_TODO,
    payload : id
  }
}

