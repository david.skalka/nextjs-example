
export const exampleInitialState : State = {
  items: [{title: 'ipsum dolor sit amet,', text: 'ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut', id: 1}]
}

export type TodoItem = {id: number, title: string, text: string}

export type State = {
  items: TodoItem[],
}

export const actionTypes = {
  ADD: 'ADD_TODO',
  DELETE_TODO: 'DELETE_TODO',
}

export const reducer = (state: State = exampleInitialState, action: any) => {
  switch (action.type) {
    case actionTypes.ADD:
        
        let nextId=0;
        if(state.items.length>0){
          nextId = Math.max(...state.items.map(e=> e.id))+1;
      
        }

      return { 
        ...state, items: state.items.concat({...action.payload, id: nextId})
    }
    case actionTypes.DELETE_TODO:
      return { 
        ...state, items: state.items.filter(e=>e.id!==action.payload)
    }
    default: return state
  }
}
